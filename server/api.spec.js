const expect = require('chai').expect;
const Promise = require('bluebird');
const validateMiddleware = require('express-jsonschema').validate;

const api = require('./api');

function validate(schema, value) {
  const passedSchema = { body: schema };
  const passedValue = { body: value };
  return Promise.fromCallback(next => validateMiddleware(passedSchema)(passedValue, {}, next));
}

describe('#server #unit #api', () => {
  it('sanity check: should fail with a bad validation', async () => {
    await expect(validate({ type: 'object' }, 'string')).to.be.rejected;
  });

  it('sanity check: should succeed with a good validation', async () => {
    await expect(validate({ type: 'object' }, { })).to.be.fulfilled;
  });

  it('should fail /login validation without username', async () => {
    await expect(validate(api.LoginSchema, {
      password: 'asdf1234',
    })).to.be.rejected;
  });

  it('should fail /login validation without username', async () => {
    await expect(validate(api.LoginSchema, {
      password: 'asdf1234',
    })).to.be.rejected;
  });

  it('should succeed /login validation with username and password', async () => {
    await expect(validate(api.LoginSchema, {
      username: 'test',
      password: 'asdf1234',
    })).to.be.fulfilled;
  });

  it('should succeed /register validation with a photoUrl', async () => {
    await expect(validate(api.RegisterSchema, {
      username: 'test',
      password: 'asdf1234',
      email: 'test@test.com',
      role: 'user',
      name: 'Test User',
      photoUrl: 'http://asdf.com/photo',
    })).to.be.fulfilled;
  });

  it('should succeed /register validation with null photoUrl', async () => {
    await expect(validate(api.RegisterSchema, {
      username: 'test',
      password: 'asdf1234',
      email: 'test@test.com',
      role: 'user',
      name: 'Test User',
      photoUrl: null,
    })).to.be.fulfilled;
  });

  it('should succeed /register validation without a photoUrl', async () => {
    await expect(validate(api.RegisterSchema, {
      username: 'test',
      password: 'asdf1234',
      email: 'test@test.com',
      role: 'user',
      name: 'Test User',
    })).to.be.fulfilled;
  });

  it('should succeed to update /user with [id, name]', async () => {
    await expect(validate(api.ManageUserUpdateSchema, {
      id: '59c67a560aacdf2290e1bb67',
      name: 'Test User',
    })).to.be.fulfilled;
  });

  it('should succeed to update /user with [id, name, email, photoUrl = null]', async () => {
    await expect(validate(api.ManageUserUpdateSchema, {
      id: '59c67a560aacdf2290e1bb67',
      name: 'Test User',
      email: 'test@test.com',
      photoUrl: null,
    })).to.be.fulfilled;
  });

  it('should succeed to update /user with [id, password]', async () => {
    await expect(validate(api.ManageUserUpdateSchema, {
      id: '59c67a560aacdf2290e1bb67',
      password: 'asdf1234',
    })).to.be.fulfilled;
  });

  it('should fail to update /user with [name]', async () => {
    await expect(validate(api.ManageUserUpdateSchema, {
      name: 'Test User',
    })).to.be.rejected;
  });

  it('should fail to update /user with [password]', async () => {
    await expect(validate(api.ManageUserUpdateSchema, {
      password: 'asdf1234',
    })).to.be.rejected;
  });

  it('should fail to update /user with [id, name, password]', async () => {
    await expect(validate(api.ManageUserUpdateSchema, {
      id: '59c67a560aacdf2290e1bb67',
      name: 'Test User',
      password: 'asdf1234',
    })).to.be.rejected;
  });

  it('succeed to get /user by [id]', async () => {
    await expect(validate(api.UserGetSchema, {
      id: '59c67a560aacdf2290e1bb67',
    })).to.be.fulfilled;
  });

  it('succeed to get /user by [order]', async () => {
    await expect(validate(api.UserGetSchema, {
      order: 'username',
    })).to.be.fulfilled;
  });

  it('succeed to get /user by [order, offset]', async () => {
    await expect(validate(api.UserGetSchema, {
      order: 'username',
      offset: 1,
    })).to.be.fulfilled;
  });

  it('should fail to get /user by [id, order]', async () => {
    await expect(validate(api.UserGetSchema, {
      id: '59c67a560aacdf2290e1bb67',
      order: 'username',
    })).to.be.rejected;
  });

  it('should succeed to update user /repair by [isCompleted = true]', async () => {
    await expect(validate(api.UserRepairUpdateSchema, {
      id: '59c67a560aacdf2290e1bb67',
      isCompleted: true,
    })).to.be.fulfilled;
  });

  it('should fail to update user /repair by [isCompleted = false]', async () => {
    await expect(validate(api.UserRepairUpdateSchema, {
      id: '59c67a560aacdf2290e1bb67',
      isCompleted: false,
    })).to.be.rejected;
  });
});
