const constants = require('./util/constants');
const express = require('express');
const passport = require('passport');
const validate = require('express-jsonschema').validate;
const requireRole = require('./middleware/requireRole');

const loginController = require('./controllers/login');
const registerController = require('./controllers/register');
const profileController = require('./controllers/profile');
const userController = require('./controllers/user.js');
const manageUserController = require('./controllers/manager/user');
const manageRepairController = require('./controllers/manager/repair');
const userUserController = require('./controllers/user/user');
const userRepairController = require('./controllers/user/repair');

const router = express.Router();

const LoginSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    username: {
      type: 'string',
      required: true,
      pattern: constants.validators.username.regex,
    },
    password: {
      type: 'string',
      required: true,
      pattern: constants.validators.password.regex,
    },
  },
};

const RegisterSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    username: {
      type: 'string',
      required: true,
      pattern: constants.validators.username.regex,
    },
    password: {
      type: 'string',
      required: true,
      pattern: constants.validators.password.regex,
    },
    email: {
      type: 'string',
      required: true,
      pattern: constants.validators.email.regex,
    },
    role: {
      type: 'string',
      required: true,
      enum: constants.roles,
    },
    name: {
      type: 'string',
      required: true,
      pattern: constants.validators.name.regex,
    },
    photoUrl: {
      type: ['string', 'null'],
    },
  },
};

const AvailableSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    username: {
      type: 'string',
      pattern: constants.validators.username.regex,
    },
    email: {
      type: 'string',
      pattern: constants.validators.email.regex,
    },
  },
};

const ProfileUpdateSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    name: {
      type: 'string',
      pattern: constants.validators.name.regex,
    },
    email: {
      type: 'string',
      pattern: constants.validators.email.regex,
    },
    password: {
      type: 'string',
      pattern: constants.validators.password.regex,
    },
    photoUrl: {
      type: ['string', 'null'],
    },
  },
  oneOf: [{
    anyOf: [
      { required: ['name'] },
      { required: ['email'] },
      { required: ['photoUrl'] },
    ],
  }, {
    required: ['password'],
  }],
};

const UserGetSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: constants.validators.id.regex,
    },
    offset: {
      type: 'integer',
      minValue: 0,
    },
    page: {
      type: 'integer',
      minValue: 1,
    },
    order: {
      type: 'string',
      enum: ['createdOn', 'username'],
    },
  },
  oneOf: [{
    anyOf: [
      { required: ['offset'] },
      { required: ['page'] },
      { required: ['order'] },
    ],
  }, {
    required: ['id'],
  }],
};

const ManageUserCreateSchema = RegisterSchema;

const ManageUserUpdateSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: constants.validators.id.regex,
    },
    name: {
      type: 'string',
      pattern: constants.validators.name.regex,
    },
    email: {
      type: 'string',
      pattern: constants.validators.email.regex,
    },
    password: {
      type: 'string',
      pattern: constants.validators.password.regex,
    },
    photoUrl: {
      type: ['string', 'null'],
    },
    role: {
      type: 'string',
      enum: constants.roles,
    },
  },
  oneOf: [{
    anyOf: [
      { required: ['id', 'name'] },
      { required: ['id', 'email'] },
      { required: ['id', 'photoUrl'] },
      { required: ['id', 'role'] },
    ],
  }, {
    required: ['id', 'password'],
  }],
};

const ManageUserDeleteSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: constants.validators.id.regex,
      required: true,
    },
  },
};

const ManageRepairGetSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: constants.validators.id.regex,
    },
    offset: {
      type: 'integer',
      minValue: 0,
    },
    page: {
      type: 'integer',
      minValue: 1,
    },
    order: {
      type: 'string',
      enum: ['date', 'isCompleted'],
    },
    minDate: {
      type: 'string',
      format: 'date-time',
    },
    maxDate: {
      type: 'string',
      format: 'date-time',
    },
    isCompleted: {
      type: 'boolean',
    },
  },
  oneOf: [{
    anyOf: [
      { required: ['offset'] },
      { required: ['page'] },
      { required: ['order'] },
      { required: ['minDate'] },
      { required: ['maxDate'] },
      { required: ['isCompleted'] },
    ],
  }, {
    required: ['id'],
  }],
};

const ManageRepairCreateSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    title: {
      type: 'string',
      maxLength: constants.validators.title.maxLength,
    },
    date: {
      type: 'string',
      format: 'date-time',
      required: true,
    },
  },
};

const ManageRepairUpdateSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: constants.validators.id.regex,
    },
    title: {
      type: 'string',
      maxLength: constants.validators.title.maxLength,
    },
    assignee: {
      type: 'string',
      pattern: constants.validators.id.regex,
    },
    isCompleted: {
      type: 'boolean',
    },
    isApproved: {
      type: 'boolean',
    },
  },
  anyOf: [{
    oneOf: [
      { required: ['id', 'title'] },
      { required: ['id', 'assignee'] },
      { required: ['id', 'isCompleted'] },
      { required: ['id', 'isApproved'] },
    ],
  },
  { required: ['id', 'assignee', 'isCompleted'] },
  ],

};

const ManageRepairCommentSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: constants.validators.id.regex,
    },
    body: {
      type: 'string',
      maxLength: constants.validators.body.maxLength,
      required: true,
    },
  },
};

const ManageRepairDeleteSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: constants.validators.id.regex,
      required: true,
    },
  },
};

const UserRepairGetSchema = ManageRepairGetSchema;

const UserRepairUpdateSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: constants.validators.id.regex,
    },
    isCompleted: {
      type: 'boolean',
      enum: [true],
      required: true,
    },
  },
};

const UserRepairCommentSchema = ManageRepairCommentSchema;

// No authorization is used for these APIs:

// Login
router.route('/login')
  .post(validate({ body: LoginSchema }), (req, res, next) => {
    loginController(req, res).then(data => res.json(data))
      .catch(next);
  });

// Register
router.route('/register')
  .post(validate({ body: RegisterSchema }), (req, res, next) => {
    registerController.register(req, res).then(data => res.json(data))
      .catch(next);
  });

// Check for available username / e-mail
router.route('/available')
  .get(validate({ query: AvailableSchema }), (req, res, next) => {
    registerController.available(req, res).then(data => res.json(data))
      .catch(next);
  });

// The APIs that follow this line use authorization:
router.use(passport.authenticate('jwt', { session: false }));

// User Profile
router.route('/profile')
  .get((req, res, next) => {
    profileController.get(req, res).then(data => res.json(data))
      .catch(next);
  });

router.route('/profile')
  .put(validate({ body: ProfileUpdateSchema }), (req, res, next) => {
    profileController.update(req, res).then(data => res.json(data))
      .catch(next);
  });

// Single User list
router.route('/user')
  .get(validate({ query: UserGetSchema }), (req, res, next) => {
    userController.get(req, res).then(data => res.json(data))
      .catch(next);
  });

// Comment to any repair
router.route('/comment')
  .post(validate({ query: ManageRepairCommentSchema }), (req, res, next) => {
    manageRepairController.comment(req, res).then(data => res.json(data))
      .catch(next);
  });


// The following APIs are restricted per user (manager) role
router.use('/manager/', requireRole('manager'));

// User Management
router.route('/manager/user')
  .get(validate({ query: UserGetSchema }), (req, res, next) => {
    manageUserController.get(req, res).then(data => res.json(data))
      .catch(next);
  });

router.route('/manager/user')
  .post(validate({ body: ManageUserCreateSchema }), (req, res, next) => {
    manageUserController.create(req, res).then(data => res.json(data))
      .catch(next);
  });

router.route('/manager/user')
  .put(validate({ body: ManageUserUpdateSchema }), (req, res, next) => {
    manageUserController.update(req, res).then(data => res.json(data))
      .catch(next);
  });

router.route('/manager/user')
  .delete(validate({ query: ManageUserDeleteSchema }), (req, res, next) => {
    manageUserController.delete(req, res).then(data => res.json(data))
      .catch(next);
  });

// Repair Management
router.route('/manager/repair')
  .get(validate({ query: ManageRepairGetSchema }), (req, res, next) => {
    manageRepairController.get(req, res).then(data => res.json(data))
      .catch(next);
  });

router.route('/manager/repair')
  .post(validate({ body: ManageRepairCreateSchema }), (req, res, next) => {
    manageRepairController.create(req, res).then(data => res.json(data))
      .catch(next);
  });

router.route('/manager/repair')
  .put(validate({ body: ManageRepairUpdateSchema }), (req, res, next) => {
    manageRepairController.update(req, res).then(data => res.json(data))
      .catch(next);
  });

router.route('/manager/repair')
  .delete(validate({ query: ManageRepairDeleteSchema }), (req, res, next) => {
    manageRepairController.delete(req, res).then(data => res.json(data))
      .catch(next);
  });

// The following APIs are restricted per user (user) role
router.use('/user/', requireRole('user'));

router.route('/user/user')
  .get(validate({ query: UserGetSchema }), (req, res, next) => {
    userUserController.get(req, res).then(data => res.json(data))
      .catch(next);
  });

// User Repair Editing
router.route('/user/repair')
  .get(validate({ query: UserRepairGetSchema }), (req, res, next) => {
    userRepairController.get(req, res).then(data => res.json(data))
      .catch(next);
  });

router.route('/user/repair')
  .put(validate({ body: UserRepairUpdateSchema }), (req, res, next) => {
    userRepairController.update(req, res).then(data => res.json(data))
      .catch(next);
  });

router.route('/user/comment')
  .post(validate({ query: UserRepairCommentSchema }), (req, res, next) => {
    userRepairController.comment(req, res).then(data => res.json(data))
      .catch(next);
  });

module.exports = {
  LoginSchema,
  RegisterSchema,
  AvailableSchema,
  ProfileUpdateSchema,
  UserGetSchema,
  ManageUserCreateSchema,
  ManageUserUpdateSchema,
  ManageUserDeleteSchema,
  ManageRepairGetSchema,
  ManageRepairCreateSchema,
  ManageRepairUpdateSchema,
  ManageRepairDeleteSchema,
  UserRepairGetSchema,
  UserRepairUpdateSchema,
  router,
};
