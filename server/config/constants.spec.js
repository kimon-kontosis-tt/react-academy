const expect = require('chai').expect;

const constants = require('./constants');

describe('#unit #config', () => {
  it('should have valid default development environment configuration constants', () => {
    const expectedConstants = {
      env: 'development',
      app: {
        url: 'http://127.0.0.1:8080',
        host: '127.0.0.1',
        port: '8080',
        protocol: 'http',
      },
      bcrypt: {
        saltRounds: 11,
      },
      db: {
        uri: 'mongodb://localhost/react_academy_dev',
      },
      jwt: {
        secretOrKey: 'hackme',
      },
    };

    expect(constants).to.eql(expectedConstants);
  });
});
