const assert = require('assert');
const constants = require('../util/constants');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RepairSchema = new Schema({
  title: {
    type: String,
    default: '',
    required: true,
    trim: true,
  },
  date: {
    type: Date,
    required: true,
    index: true,
  },
  isApproved: {
    type: Boolean,
    required: true,
    default: false,

  },
  isCompleted: {
    type: Boolean,
    required: true,
    default: false,
  },
  assignee: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  assigner: {
    type: Schema.ObjectId,
    ref: 'User',
    required: () => Boolean(this.assignee),
    validator: v => Boolean(this.assignee) || !v,
  },
  reviewer: {
    type: Schema.ObjectId,
    ref: 'User',
    required: () => this.isApproved,
  },
  completionDate: {
    type: Date,
    required: () => this.isCompleted,
    validator: v => this.isCompleted || !v,
  },
  approvalDate: {
    type: Date,
    required: () => this.isApproved,
    validator: v => this.isApproved || !v,
  },
  comments: [{
    creationDate: {
      type: String,
      required: true,
      default: Date.now,
    },
    userId: {
      type: Schema.ObjectId,
      required: true,
      ref: 'User',
    },
    body: {
      type: String,
      default: '',
      required: true,
      trim: true,
    },
  }],
}, { versionKey: false });

assert(constants.hourDurationInMs);
assert(constants.repairDurationInHours);

RepairSchema
  .virtual('endDate')
  .get(function getEndDate() {
    const endDate = new Date(this.date);
    const hour = constants.hourDurationInMs;
    endDate.setTime(endDate.getTime() + (constants.repairDurationInHours * hour));
    return endDate;
  });

RepairSchema
  .pre('save', function checkForConflictingDates(cb) {
    // Prohibit saving in a time range that is occupied
    return mongoose.models.Repair.findOne({
      _id: { $ne: this._id },
      date: {
        $gte: this.date,
        $lt: this.endDate,
      },
    })
      .then((exists) => {
        if (exists) {
          return cb(Object.assign(new Error('The specified time range is occupied'), { statusCode: 401 }));
        }

        return cb();
      })
      .catch(cb);
  });

RepairSchema.methods.addComment = function addComment({ userId, body }) {
  this.comments.push({ userId, body });
  return this.save();
};

module.exports = mongoose.model('Repair', RepairSchema);

