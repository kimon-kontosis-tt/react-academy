/**
 * Creates an express middleware that produces an error if the authenticated role mismatched
 *
 * @param {String} role - the required user role
 * @returns {Function}  - the express middleware
 */
module.exports = function requireRole(role) {
  return (req, res, next) => {
    if (req.user.role !== role) { return next(Object.assign(new Error('Unauthorized'), { statusCode: 401 })); }
    return next();
  };
};
