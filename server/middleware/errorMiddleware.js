const config = require('../config/constants');
const winston = require('winston');

const isDev = config.env === 'development';

module.exports = function errorMiddleware() {
  // eslint-disable-next-line no-unused-vars
  return function middlewareFunction(err, req, res, next) {
    const responseData = isDev ? {
      debug: {
        message: err.message,
        stack: err.stack,
        statusCode: err.statusCode,
      },
    } : {};

    // Log the error
    winston.error(`Received error with message: ${err.message}, status: ${err.status} and stack: ${err.stack}`);

    if (err.name === 'JsonSchemaValidation') {
      // Log the error
      winston.error(`Received validation error with message: ${err.message}, req: ${req.url}, and validations: %j`,
        err.validations);

      // Set a bad request http response status
      res.status(400);

      // Include the validation error details in the response
      Object.assign(responseData, {
        error: err.message,
        isValidationError: true,
        validations: err.validations,
      });

      return res.json(responseData);
    }

    if (err.statusCode) {
      // Log the error
      winston.error(`Received error with message: ${err.message}, status: ${err.statusCode}
        and stack: ${err.stack}`);

      // More verbose logging if it is an authorization error
      if (err.statusCode === 401) {
        // Additional logging specifications
        winston.error('Request headers: %j', req.headers);
      }

      // Set the http response status provided by the error
      res.status(err.statusCode);

      // Include the error details in the response, including an optional data field to be
      // passed to the client
      Object.assign(responseData, {
        error: err.message,
        data: err.data,
      });

      return res.json(responseData);
    }

    // Internal Server Error is responded if none of the above cases are met
    // Log the error
    winston.error(`Received error with message: ${err.message}, and stack: ${err.stack}`);

    res.status(500);

    Object.assign(responseData, {
      error: 'Internal Server Error',
    });

    return res.json(responseData);
  };
};
