const assert = require('assert');
const config = require('../config/constants');
const mongoose = require('mongoose');
const passportJWT = require('passport-jwt');
const winston = require('winston');

const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwt.secretOrKey,
};

assert(jwtOptions.secretOrKey, 'Bug: JWT secret is not set');

const authorizationStrategy = new JwtStrategy(jwtOptions, ((jwtPayload, next) => {
  // log the authorization payload
  winston.debug('Authorization payload received: %j', jwtPayload);

  // Verify user is in the database and has the given role
  mongoose.models.User.findOne({
    _id: jwtPayload._id,
    role: jwtPayload.role,
  })
    // Give a response to passport
    .then(user => next(null, user || false))
    .catch(next);
}));

module.exports = authorizationStrategy;
