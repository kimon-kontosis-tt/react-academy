const api = require('./api');
const authorizationStrategy = require('./middleware/authorizationStrategy');
const bodyParser = require('body-parser');
const config = require('./config/constants');
const errorMiddleware = require('./middleware/errorMiddleware');
const express = require('express');
const http = require('http');
const mongoose = require('mongoose');
const passport = require('passport');
const path = require('path');
const Promise = require('bluebird');
const webpack = require('webpack');
const webpackConfigModule = require('../webpack.config');
const winston = require('winston');
require('./models/User.js');
require('./models/Repair.js');

const useWebpack = !process.env.NO_WEBPACK;

mongoose.Promise = Promise;

const webpackConfig = webpackConfigModule(config.env);

let compiler;
let webpackDevMiddleware;
let webpackHotMiddleware;

if (useWebpack) {
  compiler = webpack(webpackConfig);
  // eslint-disable-next-line global-require
  webpackDevMiddleware = require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath,
  });
  // eslint-disable-next-line global-require
  webpackHotMiddleware = require('webpack-hot-middleware')(compiler);
}

const port = config.app.port;

const app = express();

function startMongoose(cb) {
  mongoose.connect(config.db.uri, { useMongoClient: true });
  const db = mongoose.connection;

  db.once('open', cb);
}

function createExpress() {
  passport.use(authorizationStrategy);

  app.use(passport.initialize());

  if (useWebpack) {
    app.use(webpackDevMiddleware);

    app.use(webpackHotMiddleware);
  }

  app.use(bodyParser.json());

  app.use('/api', api.router);

  app.use(errorMiddleware());

  app.use(express.static(path.join(__dirname, '../public')));
  app.server = http.createServer(app);
}

function startExpress(cb) {
  app.server.listen(port, cb);
}

createExpress();

startMongoose((err) => {
  if (err) throw err;
  startExpress((err2) => {
    if (err2) throw err2;
    winston.info(`server listening on ${port}`);
    app.emit('hasStarted');
  });
});

module.exports = app;
