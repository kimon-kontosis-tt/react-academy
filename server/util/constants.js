const commonConstants = require('../../common/constants');

const validators = commonConstants.validators;

const gravatarOpts = {
  s: '100',
  r: 'x',
  d: 'retro',
};

const mongoErrorCodes = {
  duplicateKey: 11000,
};

const roles = ['user', 'manager'];

module.exports = {
  validators,
  gravatarOpts,
  mongoErrorCodes,
  roles,
  hourDurationInMs: 60 * 60 * 1000,
  repairDurationInHours: 1,
};
