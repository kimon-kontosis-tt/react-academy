const _ = require('lodash');
const mongoose = require('mongoose');

const userFields = ['username', 'name', 'photoUrl'];

const fallbackUser = {
  username: '*deleted*',
  name: '- deleted user -',
  photoUrl: null,
};

/**
 * Adds the user properties to each comment in an array of repairs
 *
 * @param {Array of Object} - Array with repairs objects
 * @returns {Promise(Array of Object)} - A new Array with the extended repairs objects
 */
async function addUserInfoToComments(repairs) {
  // Get all the user Ids that participate in the comments
  const userIds = _.uniq(_.flatMap(repairs, r => _.map(r.comments, 'userId')));

  // Get the user objects from mongo
  const users = await mongoose.models.User.find({ _id: userIds });

  // Construct a map of the returned user objects by id
  const userById = _.reduce(users, (map, user) => Object.assign(map, { [user._id]: user }), {});

  function getUserInfo(userId) {
    return _.pick(userById[userId] || fallbackUser, userFields);
  }

  // Return the extended array of repairs
  return _.map(repairs, r => _.defaults({
    comments: _.map(r.comments || [], c => _.defaults(getUserInfo(c.userId), c)),
  }, r));
}

module.exports = addUserInfoToComments;
