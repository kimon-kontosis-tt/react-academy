const assert = require('assert');
const config = require('../config/constants');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

/**
 * Login User Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports = async function loginCtrl(req) {
  const { username, password } = req.body;

  // Authenticate using the database
  // Compares the encrypted password with the given plaintext password
  const user = await mongoose.models.User.testLogin({ username, password });

  // The error middleware will automatically handle responding to the login rejection and logging it
  if (!user) throw Object.assign(new Error('Invalid login'), { statusCode: 401 });

  // Login successful. From now on we'll store our authorization data in the JWT token
  const payload = {
    _id: user._id,
    role: user.role,
  };

  assert(config.jwt.secretOrKey, 'Bug: JWT key is not set');

  const token = jwt.sign(payload, config.jwt.secretOrKey);

  return {
    user,
    token,
  };
};
