const constants = require('../util/constants');
const gravatar = require('gravatar');
const mongoose = require('mongoose');

/**
 * Register New User Controller
 *
 * @param {Object} req - express request object
 * @param {Object} res - express response object
 * @returns {Promise(responseData)}
 */
module.exports.register = async function registerCtrl(req, res) {
  const { photoUrl, email } = req.body;

  // the req.body fields have been checked and are restricted by the validators
  const dbPayload = Object.assign({}, req.body);

  // use gravatar if no photoUrl is provided
  dbPayload.photoUrl = photoUrl || `${gravatar.url(email, constants.gravatarOpts, false)}`;

  // define a new user object
  const newUser = new mongoose.models.User(dbPayload);
  let user;

  try {
    // Insert in the database
    user = await newUser.create();
  } catch (err) {
    // Check for duplicate
    if (err.name === 'MongoError' && err.code === constants.mongoErrorCodes.duplicateKey) {
      throw Object.assign(new Error('Resource already exists'), { statusCode: 401 });
    }
    throw err;
  }

  res.status(201);

  return {
    success: true,
    user,
  };
};

/**
 * Check Available Controler
 * Checks if username or email is available
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.available = async function availableCtrl(req) {
  const { username, email } = req.query;

  if (
    (!username && !email) ||
    (username && email)
  ) {
    throw Object.assign(new Error('Exactly one parameter must be set'), { statusCode: 400 });
  }

  const query = { };
  if (username) Object.assign(query, { username });
  if (email) Object.assign(query, { email });

  const found = await mongoose.models.User.findOne(query);

  return {
    query,
    available: !found,
  };
};
