const queryMapping = require('../../util/queryMapping');
const mongoose = require('mongoose');
const registerController = require('../register.js');

/**
 * Get all the Users, User Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.get = async function getCtrl(req) {
  const payload = req.query;

  // Convert the following payload fields to mongoose query fields
  const payloadToQueryMap = {
    id: ['_id'],
  };

  const query = queryMapping.mapPayloadToQuery(payload, payloadToQueryMap);
  const sortOpts = queryMapping.getSortOptions(payload, 'username');

  const users = await mongoose.models.User.find(query, null, sortOpts);

  return users;
};
