const mongoose = require('mongoose');

/**
 * Get User Profile Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.get = async function getCtrl(req) {
  return mongoose.models.User.findOne({ _id: req.user._id });
};
