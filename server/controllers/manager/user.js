const _ = require('lodash');
const queryMapping = require('../../util/queryMapping');
const mongoose = require('mongoose');
const registerController = require('../register.js');

/**
 * Get all the Users, Manager Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.get = async function getCtrl(req) {
  const payload = req.query;

  // Convert the following payload fields to mongoose query fields
  const payloadToQueryMap = {
    id: ['_id'],
  };

  const query = queryMapping.mapPayloadToQuery(payload, payloadToQueryMap);
  const sortOpts = queryMapping.getSortOptions(payload, 'username');

  const users = await mongoose.models.User.find(query, null, sortOpts);

  return users;
};

/**
 * Create a new User, Manager Controller
 *
 * @param {Object} req - express request object
 * @param {Object} res - express response object
 * @returns {Promise(responseData)}
 */
module.exports.create = async function createCtrl(req, res) {
  // Re-use the same controller implementation.
  // Using a different controller end-point in case the specifications changes in the future
  return registerController.register(req, res);
};

/**
 * Update a User, Manager Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.update = async function updateCtrl(req) {
  const payload = req.body;
  const fields = _.omit(payload, 'id');

  const row = await mongoose.models.User.findOne({ _id: payload.id });
  if (!row) throw Object.assign(new Error('Not found'), { statusCode: 404 });
  row.set(fields);
  await row.save();

  return {
    success: true,
  };
};

/**
 * Delete a User, Manager Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.delete = async function deleteCtrl(req) {
  const { id } = req.query;

  const row = await mongoose.models.User.find({ _id: id });
  if (!row) throw Object.assign(new Error('Not found'), { statusCode: 404 });
  await row.remove();

  return {
    success: true,
  };
};
