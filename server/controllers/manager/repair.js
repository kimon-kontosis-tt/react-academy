const _ = require('lodash');
const addUserInfoToComments = require('../../util/addUserInfoToComments');
const constants = require('../../util/constants');
const queryMapping = require('../../util/queryMapping');
const moment = require('moment');
const mongoose = require('mongoose');

/**
 * Get all the Repairs, Manager Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.get = async function getCtrl(req) {
  const payload = req.query;

  // Convert the following payload fields to mongoose query fields
  const payloadToQueryMap = {
    minDate: ['date.$gte', d => moment(d).toDate()],
    maxDate: ['date.$lte', d => moment(d).toDate()],
    isCompleted: [],
    id: ['_id'],
  };

  const query = queryMapping.mapPayloadToQuery(payload, payloadToQueryMap);
  const sortOpts = queryMapping.getSortOptions(payload, 'date');

  const repairs = await mongoose.models.Repair.find(query, null, sortOpts);
  const mapRepairsToObjects = _.map(repairs, r => r.toObject());

  const extendedRepairs = await addUserInfoToComments(mapRepairsToObjects);

  return extendedRepairs;
};

/**
 * Create a new Repair, Manager Controller
 *
 * @param {Object} req - express request object
 * @param {Object} res - express response object
 * @returns {Promise(responseData)}
 */
module.exports.create = async function createCtrl(req, res) {
  // the req.body fields have been checked and are restricted by the validators
  const dbPayload = Object.assign({}, req.body);

  dbPayload.comments = [];

  // define a new repair object
  const newRepair = new mongoose.models.Repair(dbPayload);
  let repair;

  try {
    // Insert in the database
    repair = await newRepair.save();
  } catch (err) {
    // Check for duplicate
    if (err.name === 'MongoError' && err.code === constants.mongoErrorCodes.duplicateKey) {
      throw Object.assign(new Error('Resource already exists'), { statusCode: 401 });
    }
    throw err;
  }

  res.status(201);

  return {
    success: true,
    repair,
  };
};

/**
 * Update a Repair, Manager Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.update = async function updateCtrl(req) {
  const payload = req.body;
  const fields = _.omit(payload, 'id');

  const row = await mongoose.models.Repair.findOne({ _id: payload.id });
  if (!row) throw Object.assign(new Error('Not found'), { statusCode: 404 });
  row.set(fields);
  await row.save();

  return {
    success: true,
  };
};

/**
 * Add a Comment to a Repair, Manager Controller
 *
 * @param {Object} req - express request object
 * @param {Object} res - express response object
 * @returns {Promise(responseData)}
 */
module.exports.comment = async function commentCtrl(req, res) {
  const { id, body } = req.body;

  const row = await mongoose.models.Repair.findOne({ _id: id });
  if (!row) throw Object.assign(new Error('Not found'), { statusCode: 404 });
  await row.addComment({
    userId: req.user._id,
    body,
  });

  res.status(201);

  return {
    success: true,
  };
};

/**
 * Delete a Repair, Manager Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.delete = async function deleteCtrl(req) {
  const { id } = req.query;

  const row = await mongoose.models.Repair.find({ _id: id });
  if (!row) throw Object.assign(new Error('Not found'), { statusCode: 404 });
  await row.remove();

  return {
    success: true,
  };
};
