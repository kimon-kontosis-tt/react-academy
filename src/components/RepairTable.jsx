import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';
import NameWithAvatar from './NameWithAvatar';

export default class RepairTable extends React.Component {
  renderItem(key, value, row) {
    if (key === 'isCompleted') {
      return <input type="checkbox" readOnly checked={Boolean(value)} />;
    }

    if (key === 'assignee') {
      if (!value) return null;
      const user = _.find(this.props.users.userItems, { _id: value });
      if (!user) return null;

      return <NameWithAvatar value={user.name} photoUrl={user.photoUrl} />;
    }

    return value;
  }


  render() {
    return (
      <mui.Table
        onRowSelection={(arrSelected) => {
          if (arrSelected.length) {
            this.props.onSelect(this.props.rows[arrSelected[0]]);
          }
        }}
      >
        {<mui.TableHeader>
          <mui.TableRow>
            <mui.TableHeaderColumn>Date</mui.TableHeaderColumn>
            <mui.TableHeaderColumn>[title]</mui.TableHeaderColumn>
            <mui.TableHeaderColumn>Completed</mui.TableHeaderColumn>
            <mui.TableHeaderColumn>Assignee</mui.TableHeaderColumn>
          </mui.TableRow>
        </mui.TableHeader>}
        <mui.TableBody>
          {this.props.rows.map((r, idx) => (
            <mui.TableRow
              key={r._id}
              selected={r._id === this.props.selected}
            >
              {['date', 'title', 'isCompleted', 'assignee'].map(key => (
                <mui.TableRowColumn key={key} >
                  {this.renderItem(key, r[key], r)}
                </mui.TableRowColumn>
              ))}
            </mui.TableRow>
          ))}
        </mui.TableBody>
      </mui.Table>
    );
  }
}

RepairTable.propTypes = {
  rows: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
  selected: PropTypes.string,
  users: PropTypes.object.isRequired,
};

