import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';
import NameWithAvatar from './NameWithAvatar';

export default class UserTable extends React.Component {
  renderItem(key, value, row) {
    if (key === 'name') {
      return (
        <NameWithAvatar value={value} photoUrl={row.photoUrl} />
      );
    }

    return value;
  }


  render() {
    return (
      <mui.Table
        onRowSelection={(arrSelected) => {
          if (arrSelected.length) {
            this.props.onSelect(this.props.rows[arrSelected[0]]);
          }
        }}
      >
        <mui.TableHeader>
          <mui.TableRow>
            <mui.TableHeaderColumn>Name</mui.TableHeaderColumn>
            <mui.TableHeaderColumn>username</mui.TableHeaderColumn>
            <mui.TableHeaderColumn>role</mui.TableHeaderColumn>
          </mui.TableRow>
        </mui.TableHeader>
        <mui.TableBody>
          {this.props.rows.map((r, idx) => (
            <mui.TableRow
              key={r._id}
              selected={r._id === this.props.selected}
            >
              {['name', 'username', 'role'].map(key => (
                <mui.TableRowColumn key={key} >
                  {this.renderItem(key, r[key], r)}
                </mui.TableRowColumn>
              ))}
            </mui.TableRow>
          ))}
        </mui.TableBody>
      </mui.Table>
    );
  }
}

UserTable.propTypes = {
  rows: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
  selected: PropTypes.string,
};

