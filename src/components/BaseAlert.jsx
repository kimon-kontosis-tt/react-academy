import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';

export default class BaseAlert extends React.Component {
  render() {
    return (
      <mui.Dialog
        actions={[
          <mui.FlatButton
            label="Ok"
            primary
            onClick={this.props.onClose}
          />,
        ]}
        modal={false}
        open={Boolean(this.props.message)}
        onRequestClose={this.props.onClose}
      >{this.props.message || ''}</mui.Dialog>
    );
  }
}

BaseAlert.propTypes = {
  message: PropTypes.string,
  onClose: PropTypes.func.isRequired,
};

BaseAlert.defaultProps = {
  message: '',
};
