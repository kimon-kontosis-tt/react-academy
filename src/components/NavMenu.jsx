import React from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import MenuItem from 'material-ui/MenuItem';


export default class NavMenu extends React.Component {
  goTo(url) {
    this.context.router.push(url);
  }

  render() {
    return (
      <AppBar
        title="React Academy Auto Repair"
        iconElementRight={
          <IconMenu
            iconButtonElement={
              <IconButton><MoreVertIcon /></IconButton>
            }
            targetOrigin={{ horizontal: 'right', vertical: 'top' }}
            anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
          >
            {this.props.options.map(op => (
              <MenuItem
                key={op.url}
                primaryText={op.label}
                onClick={() => this.goTo(op.url)}
              />
            ))}
          </IconMenu>
        }
      />
    );
  }
}

NavMenu.propTypes = {
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
};

NavMenu.contextTypes = {
  router: PropTypes.object.isRequired,
};
