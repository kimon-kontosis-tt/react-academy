import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';
import ComposedComponent from 'react-schema-form/lib/ComposedComponent';

class DatePickerFormElement extends React.Component {
  constructor(props) {
    super(props);
    this.onDatePicked = this.onDatePicked.bind(this);
  }


  onDatePicked(empty, date) {
    this.props.onChangeValidate({
      target: {
        value: date,
      },
    });
  }

  render() {
    let value = null;
    if (this.props && this.props.value) {
      value = this.props.value;
    }

    return (
      <div style={{ width: '100%', display: 'block' }} className={this.props.form.htmlClass}>
        <mui.DatePicker
          mode={'landscape'}
          autoOk
          floatingLabelText={this.props.form.title}
          hintText={this.props.form.title}
          onChange={this.onDatePicked}
          onShow={null}
          onDismiss={null}
          value={value}
          disabled={this.props.form.readonly}
          style={this.props.form.style || { width: '100%' }}
        />

      </div>
    );
  }
}

DatePickerFormElement.propTypes = {
  form: PropTypes.object.isRequired,
  value: PropTypes.any,
};

export default ComposedComponent(DatePickerFormElement);
