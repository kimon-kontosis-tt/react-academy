import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';
import { List, ListItem, makeSelectable } from 'material-ui/List';
import ComposedComponent from 'react-schema-form/lib/ComposedComponent';

let SelectableList = makeSelectable(List);

function wrapState(ComposedComponentFn) {
  return class SelectableList extends React.Component {
    static propTypes = {
      children: PropTypes.node.isRequired,
      defaultValue: PropTypes.number.isRequired,
    };

    componentWillMount() {
      this.setState({
        selectedIndex: this.props.defaultValue,
      });
    }

    handleRequestChange = (event, index) => {
      this.setState({
        selectedIndex: index,
      });
    };

    render() {
      return (
        <ComposedComponentFn
          value={this.state.selectedIndex}
          onChange={this.handleRequestChange}
        >
          {this.props.children}
        </ComposedComponentFn>
      );
    }
  };
}

SelectableList = wrapState(SelectableList);

class MobileTearSheet extends React.Component {
  render() {
    const styles = {
      root: {
        float: 'left',
        marginBottom: 24,
        marginRight: 24,
        width: 360,

      },

      container: {
        borderBottom: 'none',
        height: this.props.height,
        overflow: 'hidden',
      },

      bottomTear: {
        display: 'block',
        position: 'relative',
        marginTop: -10,
        width: 360,
      },
    };

    return (
      <div style={styles.root}>
        <div style={styles.container}>
          {this.props.children}
        </div>
        <img style={styles.bottomTear} src="images/bottom-tear.svg" />
      </div>
    );
  }
}

MobileTearSheet.propTypes = {
  height: React.PropTypes.number,
};

MobileTearSheet.defaultProps = {
  height: 500,
};

class PersonPickerFormElement extends React.Component {
  constructor(props) {
    super(props);
    this.onClicked = this.onClicked.bind(this);

    this.state = {
      open: false,
    };
  }

  onClicked(val) {
    this.setState({
      open: false,
    });

    if (val === 0) {
      return this.props.onChangeValidate({
        target: {
          value: {
            list: this.props.value.list,
            selected: '',
          },
        },
      });
    }

    const selectedId = this.props.value.list[val - 1]._id;

    return this.props.onChangeValidate({
      target: {
        value: {
          list: this.props.value.list,
          selected: selectedId,
        },
      },
    });
  }


  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const list = this.props.value.list;
    const selected = this.props.value.selected;
    let assignee = _.find(list, { _id: selected });
    if (!assignee) {
      assignee = {
        name: 'Unassigned',
      };
    }

    return (
      <div>
        Assigned to:
        <List>
          <ListItem
            onTouchTap={this.handleTouchTap}
            primaryText={assignee.name}
            leftAvatar={assignee.photoUrl ? (<mui.Avatar src={assignee.photoUrl} />) : <span />}
          />
        </List>
        <br />
        <MobileTearSheet>
          <mui.Popover
            open={this.state.open}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
            targetOrigin={{ horizontal: 'left', vertical: 'top' }}
            onRequestClose={this.handleRequestClose}
          >
            <SelectableList
              defaultValue={0}
            >{
                [
                  <mui.Subheader key="subheader" >Assign</mui.Subheader>,
                  <ListItem
                    key="none"
                    value="none"
                    primaryText="Unassigned"
                    onTouchTap={this.onClicked.bind(this, 0)}
                  />,
                ].concat(list.map((person, idx) => (
                  <ListItem
                    key={person._id}
                    value={person._id}
                    primaryText={person.name}
                    leftAvatar={<mui.Avatar src={person.photoUrl} />}
                    onTouchTap={this.onClicked.bind(this, idx + 1)}
                  />
                )))

              }
            </SelectableList>
          </mui.Popover>
        </MobileTearSheet>
      </div>
    );
  }
}

PersonPickerFormElement.propTypes = {
  value: PropTypes.object,
};

PersonPickerFormElement.defaultProps = {
  value: {
    list: [],
    selected: null,
  },
};

export default ComposedComponent(PersonPickerFormElement);
