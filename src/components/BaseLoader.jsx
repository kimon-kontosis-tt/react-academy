import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';

export default class BaseLoader extends React.Component {
  render() {
    return (
      <div>
        <mui.LinearProgress
          style={this.props.enabled ? {} : { display: 'none' }}
          mode="indeterminate"
        />
        <mui.LinearProgress
          style={!this.props.enabled ? {} : { display: 'none' }}
          mode="determinate"
        />
      </div>
    );
  }
}

BaseLoader.propTypes = {
  enabled: PropTypes.bool.isRequired,
};
