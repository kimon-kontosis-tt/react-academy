import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';
import { utils, SchemaForm } from 'react-schema-form';
import DatePickerFormElement from './extensions/DatePickerFormElement';
import TimePickerFormElement from './extensions/TimePickerFormElement';
import PersonPickerFormElement from './extensions/PersonPickerFormElement';

export default class BaseForm extends React.Component {
  constructor(props) {
    super(props);
    this.onModelChange = this.onModelChange.bind(this);
    this.onValidate = this.onValidate.bind(this);

    this.state = {
      validationResult: {},
      model: props.model,
      validationAlertOpen: false,
    };
  }

  onModelChange(key, value) {
    this.setState({ model: _.defaults({ [key]: value }, this.state.model) });
  }

  onValidate() {
    this.setState({ model: this.state.model });
    const result = utils.validateBySchema(this.props.schema, this.state.model);
    this.setState({ validationResult: result });

    if (result.error && this.props.validate) return this.handleOpenValidationAlert();

    return this.props.onSubmit(this.state.model);
  }

  handleCloseValidationAlert() {
    this.setState({ validationAlertOpen: false });
  }

  handleOpenValidationAlert() {
    this.setState({ validationAlertOpen: true });
  }

  render() {
    const mapper = {
      date2: DatePickerFormElement,
      time2: TimePickerFormElement,
      person2: PersonPickerFormElement,
    };

    this.form = (
      <SchemaForm
        schema={this.props.schema}
        form={this.props.form}
        model={this.props.model}
        onModelChange={this.onModelChange}
        mapper={mapper}
      />
    );

    return (
      <div>
        {this.form}
        <br />
        <mui.RaisedButton primary label={this.props.submitLabel} onTouchTap={this.onValidate} />

        <mui.Dialog
          actions={[
            <mui.FlatButton
              label="Ok"
              primary
              onClick={() => this.handleCloseValidationAlert()}
            />,
          ]}
          modal={false}
          open={this.state.validationAlertOpen}
          onRequestClose={() => this.handleCloseValidationAlert()}
        >{_.get(this.state.validationResult, 'error.message')}</mui.Dialog>

      </div>
    );
  }
}

BaseForm.propTypes = {
  schema: PropTypes.object.isRequired,
  form: PropTypes.array.isRequired,
  submitLabel: PropTypes.string.isRequired,
  model: PropTypes.object,
  validate: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
};

BaseForm.defaultProps = {
  model: {},
  validate: true,
};

