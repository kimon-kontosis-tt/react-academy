import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';

export default class NameWithAvatar extends React.Component {
  render() {
    return (
      <p style={{ display: 'inline-block', verticalAlign: 'baseline' }} >
        <mui.Avatar
          src={this.props.photoUrl}
          size={30}
          style={{
            position: 'relative',
            top: '-8px',
            marginRight: 20,
            float: 'left',
          }}
        />
        {this.props.value}
      </p>
    );
  }
}

NameWithAvatar.propTypes = {
  value: PropTypes.string.isRequired,
  photoUrl: PropTypes.string.isRequired,
};
