const initialState = {
  selected: '',
  userItems: [],
};

export default function users(state = initialState, action) {
  switch (action.type) {
    case 'USER_LIST_SUCCESS':
      return Object.assign({}, state, {
        userItems: action.payload.data,
      });

    case 'USER_SELECT':
      return Object.assign({}, state, {
        selected: action.payload,
      });


    default:
      return state;
  }
}
