// The hashHistory's current route does not participate in the redux store state by design
// Here, we modify our hash history route upon certain events, making the reducer, somehow impure
// However, the reducer state is not affected by these modifications so there is no logic problem
import { hashHistory } from 'react-router';

// The sessionStorage is additionally modified in certain events, again making the reducer impure
// However, the reducer state is not affected by these modifications so there is no logic problem
const initialState = {
  role: sessionStorage.getItem('role') || '',
  user: sessionStorage.getItem('user') || {},
};

export default function profile(state = initialState, action) {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      sessionStorage.setItem('user', action.payload.data.user);
      sessionStorage.setItem('role', action.payload.data.user.role);
      sessionStorage.setItem('token', action.payload.data.token);
      setImmediate(() => hashHistory.push(`/${action.payload.data.user.role}`));

      return {
        role: action.payload.data.user.role,
        user: action.payload.data.user,
      };

    case 'REGISTER_SUCCESS':
      hashHistory.push('/');
      return state;

    case 'USER_UPDATE_SUCCESS':
      setImmediate(() => hashHistory.push(`/${state.role}/users/list`));
      return state;

    case 'LOGOUT':
      sessionStorage.setItem('user', {});
      sessionStorage.setItem('role', '');
      sessionStorage.setItem('token', '');

      return {
        role: '',
        user: {},
      };

    default:
      return state;
  }
}
