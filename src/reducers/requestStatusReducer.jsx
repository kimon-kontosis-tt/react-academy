import _ from 'lodash';

const initialState = {
  isLoading: false,
  error: '',
};

export default function requestStatus(state = initialState, action) {
  // specific cases first
  switch (action.type) {
    case 'ALERT':
      return {
        isLoading: false,
        error: action.payload.message,
      };

    case 'CONFIRM_ALERT':
      return {
        isLoading: false,
        error: '',
      };

    default:
  }

  if (_.get(action, 'payload.request')) {
    // A generic REST request
    return {
      isLoading: false,
      error: '',
    };
  }

  if (_.get(action, 'error.response.data')) {
    // A generic REST failure
    return {
      isLoading: true,
      error: action.error.response.data.error || 'Server Error',
    };
  }

  if (_.get(action, 'payload.data')) {
    // A generic REST success
    return {
      isLoading: false,
      error: '',
    };
  }

  return state;
}
