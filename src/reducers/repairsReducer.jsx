const initialState = {
  selected: '',
  repairItems: [],
};

export default function repairs(state = initialState, action) {
  switch (action.type) {
    case 'REPAIR_LIST_SUCCESS':
      return Object.assign({}, state, {
        repairItems: action.payload.data,
      });

    case 'REPAIR_SELECT':
      return Object.assign({}, state, {
        selected: action.payload,
      });


    default:
      return state;
  }
}
