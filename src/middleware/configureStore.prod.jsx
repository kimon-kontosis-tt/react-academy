import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import * as reducers from '../reducers';
import axiosMiddleware from './configureAxiosMiddleware';

const enhancer = applyMiddleware(thunk, axiosMiddleware);

export default function configureStore(initialState) {
  const store = createStore(combineReducers(reducers), initialState, enhancer);
  return store;
}
