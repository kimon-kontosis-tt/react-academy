/* eslint global-require:0,import/no-extraneous-dependencies:0 */

import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { persistState } from 'redux-devtools';
import DevTools from '../containers/DevTools';
import * as reducers from '../reducers';
import axiosMiddleware from './configureAxiosMiddleware';

const logger = createLogger();

function getDebugSessionKey() {
  const matches = window.location.href.match(/[?&]debug_session=([^&#]+)\b/);
  return (matches && matches.length > 0) ? matches[1] : null;
}

const enhancer = compose(
  applyMiddleware(thunk, logger, axiosMiddleware),
  DevTools.instrument(),
  persistState(getDebugSessionKey()),
);


export default function configureStore(initialState) {
  const store = createStore(combineReducers(reducers), initialState, enhancer);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const reducersModule = require('../reducers');
      store.replaceReducer(combineReducers(reducersModule.default || reducersModule));
    });
  }

  return store;
}
