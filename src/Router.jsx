import React from 'react';
import { IndexRoute, Router, Route, hashHistory } from 'react-router';
import App from './App';
import * as Pages from './containers/Pages';

export default function render() {
  return (
    <Router history={hashHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={Pages.Login} />
        <Route path="about" component={Pages.About} />
        <Route path="register" component={Pages.Register} />
        <Route path="user" component={Pages.User.EnsureLoggedIn}>
          <IndexRoute component={Pages.User.Dashboard} />
          <Route path="repairs/list" component={Pages.User.ListRepairs} />
          <Route path="repairs/view/:id" component={Pages.User.ViewRepair} />
          <Route path="profile/view" component={Pages.Profile.View} />
          <Route path="profile/password" component={Pages.Profile.ChangePassword} />
        </Route>
        <Route path="manager" component={Pages.Manager.EnsureLoggedIn}>
          <IndexRoute component={Pages.Manager.Dashboard} />
          <Route path="repairs/list" component={Pages.Manager.ListRepairs} />
          <Route path="repairs/create" component={Pages.Manager.CreateRepair} />
          <Route path="repairs/view/:id" component={Pages.Manager.ViewRepair} />
          <Route path="users/list" component={Pages.Manager.ListUsers} />
          <Route path="users/view/:id" component={Pages.Manager.ViewUser} />
          <Route path="profile/view" component={Pages.Profile.View} />
          <Route path="profile/password" component={Pages.Profile.ChangePassword} />
        </Route>
      </Route>
    </Router>
  );
}
