import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';
import constants from '../../common/constants';
import BaseForm from '../components/BaseForm';


const schema = {
  type: 'object',
  title: 'Create Repair',
  properties: {
    date: {
      title: 'Date',
      type: 'object',
    },
    time: {
      title: 'Time',
      type: 'object',
    },
    title: {
      title: 'Optional title',
      type: 'string',
    },
  },
  required: ['date', 'time'],
};

const form = [
  { key: 'date', type: 'date2' },
  { key: 'time', type: 'time2' },
  'title',
  'isCompleted',
];

export default class CreateRepair extends React.Component {
  render() {
    return (
      <mui.Paper style={{ padding: '10px 100px 200px 100px' }}>
        <h2>Create Repair</h2>
        <hr />
        <BaseForm
          schema={schema}
          form={form}
          submitLabel="Add"
          onSubmit={this.props.onSubmit}
        />
      </mui.Paper>
    );
  }
}

CreateRepair.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};
