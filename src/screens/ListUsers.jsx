import React from 'react';
import PropTypes from 'prop-types';
import { hashHistory } from 'react-router';
import * as mui from 'material-ui';
import UserTable from '../components/UserTable';

const style = {
  margin: 12,
};

export default class ListUsers extends React.Component {
  constructor(props) {
    super(props);
    this.edit = this.edit.bind(this);
  }

  componentDidMount() {
    this.props.onLoad();
  }

  edit() {
    hashHistory.push(`/manager/users/view/${this.props.users.selected}`);
  }

  render() {
    return (
      <mui.Paper style={{ padding: '10px 100px 200px 100px' }}>
        <h2>Users</h2>
        <hr />
        <mui.RaisedButton
          label="Delete Selected"
          primary
          style={style}
          onClick={this.del}
        />
        <mui.RaisedButton
          label="Edit Selected"
          secondary
          style={style}
          onClick={this.edit}
        />

        <UserTable
          rows={this.props.users.userItems}
          onSelect={this.props.onSelect}
          selected={this.props.users.selected}
        />

      </mui.Paper>
    );
  }
}

ListUsers.propTypes = {
  onLoad: PropTypes.func.isRequired,

};
