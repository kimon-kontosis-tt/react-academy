import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { hashHistory } from 'react-router';
import * as mui from 'material-ui';
import constants from '../../common/constants';
import BaseForm from '../components/BaseForm';


const schema = {
  type: 'object',
  title: 'Register',
  properties: {
    username: {
      title: 'Username',
      type: 'string',
      readonly: true,
      pattern: constants.validators.username.regex,
    },
    email: {
      title: 'e-mail',
      type: 'string',
      readonly: true,
      pattern: constants.validators.email.regex,
    },
    name: {
      title: 'Name',
      type: 'string',
      pattern: constants.validators.name.regex,
    },
    photoUrl: {
      title: 'Photo Url',
      type: 'string',
    },
    role: {
      title: 'Role',
      type: 'string',
      enum: ['user', 'manager'],
    },
  },
  required: ['name'],
};

const form = [
  { key: 'username', readonly: true },
  { key: 'email', readonly: true },
  'name',
  'photoUrl',
  'role',
];

export default class ViewUser extends React.Component {
  render() {
    const users = this.props.users.userItems;
    const id = this.props.users.selected;
    const selected = _.find(users, { _id: id });
    if (!selected) {
      // The id specified does not exist
      return hashHistory.push('/manager/users/list');
    }
    const model = _.pick(selected, ['username', 'email', 'name', 'photoUrl', 'role']);

    return (
      <mui.Paper style={{ padding: '10px 100px 200px 100px' }}>
        <h2>User Information</h2>
        <hr />
        <BaseForm
          schema={schema}
          form={form}
          model={model}
          submitLabel="Update"
          onSubmit={this.props.onSubmit}
        />
      </mui.Paper>
    );
  }
}

ViewUser.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};
