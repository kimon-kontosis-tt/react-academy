import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';
import constants from '../../common/constants';
import BaseForm from '../components/BaseForm';


const schema = {
  type: 'object',
  title: 'Register',
  properties: {
    username: {
      title: 'Username',
      type: 'string',
      pattern: constants.validators.username.regex,
    },
    name: {
      title: 'Name',
      type: 'string',
      pattern: constants.validators.name.regex,
    },
    role: {
      title: 'Role',
      type: 'string',
      enum: ['user', 'manager'],
    },
    email: {
      title: 'e-mail',
      type: 'string',
      pattern: constants.validators.email.regex,
    },
    password: {
      title: 'Password',
      type: 'string',
      format: 'password',
      pattern: constants.validators.password.regex,
    },
    password2: {
      title: 'Verify Password',
      type: 'string',
      pattern: constants.validators.password.regex,
    },
  },
  required: ['username', 'password'],
};

const form = [
  'username',
  'name',
  'role',
  'email',
  { key: 'password', type: 'password' },
  { key: 'password2', type: 'password' },
];

export default class Register extends React.Component {
  render() {
    return (
      <mui.Paper style={{ padding: '10px 100px 200px 100px' }}>
        <h2>Register</h2>
        <hr />
        <BaseForm
          schema={schema}
          form={form}
          submitLabel="Register"
          onSubmit={this.props.onSubmit}
        />
      </mui.Paper>
    );
  }
}

Register.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};
