import React from 'react';
import * as mui from 'material-ui';

export default function render() {
  return (
    <mui.Paper style={{ padding: '10px 100px 200px 100px' }}>
      <h2>About</h2>
      <hr />
      <p>React Academy 2017 example</p>
      <p>Author: Kimon Kontosis</p>
    </mui.Paper>
  );
}
