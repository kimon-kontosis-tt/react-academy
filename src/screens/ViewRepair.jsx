import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { hashHistory } from 'react-router';
import * as mui from 'material-ui';
import constants from '../../common/constants';
import BaseForm from '../components/BaseForm';
import CommentForm from '../components/CommentForm';


const schema = {
  type: 'object',
  title: 'Register',
  properties: {
    title: {
      title: 'Title',
      type: 'string',
      readonly: true,
    },
    date: {
      title: 'Date and Time',
      type: 'string',
      readonly: true,
    },
    assignee: {
      title: 'Assignee',
      type: 'string',
      pattern: constants.validators.name.regex,
    },
    isCompleted: {
      title: 'Is Completed',
      type: 'boolean',
    },
  },
};

const form = [
  { key: 'title', readonly: true },
  { key: 'date', readonly: true },
  'isCompleted',
  { key: 'assignee', type: 'person2' },
];

export default class ViewUser extends React.Component {
  render() {
    const repairs = this.props.repairs.repairItems;
    const id = this.props.repairs.selected;
    const selected = _.find(repairs, { _id: id });
    if (!selected) {
      // The id specified does not exist
      return hashHistory.push('/manager/repairs/list');
    }
    const model = _.pick(selected, ['date', 'title', 'isCompleted', 'assignee']);
    model.assignee = {
      selected: model.assignee,
      list: this.props.users.userItems,
    };

    return (
      <mui.Paper style={{ padding: '10px 100px 200px 100px' }}>
        <h2>User Information</h2>
        <hr />
        <BaseForm
          schema={schema}
          form={form}
          model={model}
          submitLabel="Update"
          validate={false}
          onSubmit={this.props.onSubmit}
        />
        <CommentForm
          users={this.props.users}
          id={this.props.repairs.selected}
          item={selected}
        />

      </mui.Paper>
    );
  }
}

ViewUser.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  repairs: PropTypes.object.isRequired,
};
