import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';
import constants from '../../common/constants';
import BaseForm from '../components/BaseForm';


const schema = {
  type: 'object',
  title: 'Sign In',
  properties: {
    username: {
      title: 'Username',
      type: 'string',
      pattern: constants.validators.username.regex,
    },
    password: {
      title: 'Password',
      type: 'string',
      format: 'password',
      pattern: constants.validators.password.regex,
    },
    rememberMe: {
      title: 'Remember me',
      type: 'boolean',
      default: false,
    },
  },
  required: ['username', 'password'],
};

const form = [
  'username',
  { key: 'password', type: 'password' },
  'rememberMe',
];

export default class Login extends React.Component {
  login() {
    return (
      <div>
        <h2>Login</h2>
        <hr />
        <BaseForm
          schema={schema}
          form={form}
          submitLabel="Login"
          onSubmit={this.props.onSubmit}
        />
      </div>
    );
  }

  logout() {
    return (
      <div>
        <h2>Logout</h2>
        <hr />
        <mui.RaisedButton primary label="Logout" onTouchTap={this.props.onLogout} />
      </div>
    );
  }

  render() {
    return (
      <mui.Paper style={{ padding: '10px 100px 200px 100px' }}>
        { this.props.profile.role ? this.logout() : this.login() }
      </mui.Paper>
    );
  }
}

Login.propTypes = {
  profile: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
};
