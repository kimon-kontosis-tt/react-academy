import { connect } from 'react-redux';
import NavMenu from '../components/NavMenu';

function getOptionsByRole(role) {
  switch (role) {
    case 'user':
      return [
        { url: '/user/repairs/list', label: 'Repairs' },
        { url: '/about', label: 'About' },
        { url: '/', label: 'Logout' },
      ];

    case 'manager':
      return [
        { url: '/manager/users/list', label: 'Users' },
        { url: '/manager/repairs/list', label: 'Repairs' },
        { url: '/manager/repairs/create', label: 'Create Repair' },
        { url: '/about', label: 'About' },
        { url: '/', label: 'Logout' },
      ];

    default:
      return [
        { url: '/', label: 'Login' },
        { url: '/register', label: 'Register' },
        { url: '/about', label: 'About' },
      ];
  }
}

const mapStateToProps = state => ({
  options: getOptionsByRole(state.profile.role),
  requestStatus: state.requestStatus,
});

export default connect(
  mapStateToProps,
)(NavMenu);

