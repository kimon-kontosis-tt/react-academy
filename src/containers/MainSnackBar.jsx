import { connect } from 'react-redux';
import MainSnackBar from '../components/MainSnackBar';
import * as otherActions from '../actions/other';

const mapStateToProps = state => ({
  info: state.info,
});

const mapDispatchToProps = dispatch => ({
  onCloseInfo: () => {
    dispatch(otherActions.info(null));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainSnackBar);

