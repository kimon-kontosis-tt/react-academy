import View from './View';
import ChangePassword from './ChangePassword';

export {
  View,
  ChangePassword,
};
