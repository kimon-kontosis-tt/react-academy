import { connect } from 'react-redux';
import TODO from '../../../components/TODO';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TODO);
