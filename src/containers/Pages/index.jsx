import About from './About';
import Login from './Login';
import Register from './Register';
import * as User from './User';
import * as Manager from './Manager';
import * as Profile from './Profile';

export {
  About,
  Login,
  Register,
  User,
  Manager,
  Profile,
};
