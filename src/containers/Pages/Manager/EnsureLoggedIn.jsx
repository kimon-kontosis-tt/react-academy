import { connect } from 'react-redux';
import EnsureRole from '../../../components/EnsureRole';

const mapStateToProps = state => ({
  isLoggedIn: state.profile.role === 'manager',
});

export default connect(
  mapStateToProps,
)(EnsureRole);
