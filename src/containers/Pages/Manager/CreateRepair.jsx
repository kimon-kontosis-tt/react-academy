import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import CreateRepair from '../../../screens/CreateRepair';
import * as otherActions from '../../../actions/other';
import * as apiActions from '../../../actions/api';

const mapStateToProps = state => ({
  repairs: state.repairs,
});

const mapDispatchToProps = (disp, state) => ({
  onSubmit: model => disp((dispatch, getState) => {
    const state = getState();

    const payload = { };
    if (model.title) payload.title = model.title;

    const momentTime = moment(model.time);
    const momentDate = moment(model.date);

    const composedDate = moment({
      year: momentDate.year(),
      month: momentDate.month(),
      day: momentDate.date(),
      hour: momentTime.hours(),
      minute: momentTime.minutes(),
    });

    payload.date = composedDate.format();

    return dispatch(apiActions.repairCreate(payload));
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateRepair);
