import _ from 'lodash';
import { connect } from 'react-redux';

import ViewRepair from '../../../screens/ViewRepair';
import * as otherActions from '../../../actions/other';
import * as apiActions from '../../../actions/api';

const mapStateToProps = state => ({
  users: state.users,
  repairs: state.repairs,
});

const mapDispatchToProps = (disp, state) => ({
  onSubmit: model => disp((dispatch, getState) => {
    const state = getState();

    const payload = {
      id: state.repairs.selected,
      assignee: model.assignee.selected,
      isCompleted: model.isCompleted,
    };

    return dispatch(apiActions.repairUpdate(payload));
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewRepair);
