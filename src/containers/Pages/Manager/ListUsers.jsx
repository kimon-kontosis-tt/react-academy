import { connect } from 'react-redux';
import ListUsers from '../../../screens/ListUsers';
import * as otherActions from '../../../actions/other';
import * as apiActions from '../../../actions/api';

const mapStateToProps = state => ({
  users: state.users,
});


const mapDispatchToProps = (disp, state) => ({
  onLoad: () => disp((dispatch, getState) => {
    const state = getState();

    // TODO: consider including filters from state.users.filters
    dispatch(apiActions.userList({
      order: 'username',
    }));
  }),
  onSelect: row => disp(otherActions.userSelect(row._id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListUsers);
