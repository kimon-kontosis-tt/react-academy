import _ from 'lodash';
import { connect } from 'react-redux';

import ViewUser from '../../../screens/ViewUser';
import * as otherActions from '../../../actions/other';
import * as apiActions from '../../../actions/api';

const mapStateToProps = state => ({
  users: state.users,
});

const mapDispatchToProps = (disp, state) => ({
  onSubmit: model => disp((dispatch, getState) => {
    const state = getState();

    const payload = _.defaults({
      id: state.users.selected,
    }, _.omit(model, ['username', 'email']));

    return dispatch(apiActions.userUpdate(payload));
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewUser);
