import { connect } from 'react-redux';
import Login from '../../screens/Login';
import * as otherActions from '../../actions/other';
import * as apiActions from '../../actions/api';

const mapStateToProps = state => ({
  profile: state.profile,
});

const mapDispatchToProps = (disp, state) => ({
  onSubmit: model => disp((dispatch, getState) => {
    const state = getState();

    if (state.profile.role) {
      // already logged in
      return;
    }

    dispatch(apiActions.login(model));
  }),
  onLogout: () => disp(otherActions.logout()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
