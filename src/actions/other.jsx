
export function logout() {
  return {
    type: 'LOGOUT',
    payload: {
    },
  };
}

export function info(message) {
  return {
    type: 'INFO',
    payload: {
      message,
    },
  };
}

export function alert(message) {
  return {
    type: 'ALERT',
    payload: {
      message,
    },
  };
}

export function confirmAlert() {
  return {
    type: 'CONFIRM_ALERT',
    payload: {
    },
  };
}

export function userSelect(key) {
  return {
    type: 'USER_SELECT',
    payload: key,
  };
}

export function repairSelect(key) {
  return {
    type: 'REPAIR_SELECT',
    payload: key,
  };
}
