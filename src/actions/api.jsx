
export function login(args) {
  return {
    type: 'LOGIN',
    payload: {
      request: {
        url: '/login',
        method: 'post',
        data: args,
      },
    },
  };
}

export function register(args) {
  return {
    type: 'REGISTER',
    payload: {
      request: {
        url: '/register',
        method: 'post',
        data: args,
      },
    },
  };
}

export function repairCreate(args) {
  return {
    type: 'REPAIR_CREATE',
    payload: {
      request: {
        url: '/manager/repair',
        method: 'post',
        data: args,
        headers: { Authorization: `Bearer ${sessionStorage.getItem('token')}` },
      },
    },
  };
}

export function userList(args) {
  return {
    type: 'USER_LIST',
    payload: {
      request: {
        url: `/${sessionStorage.getItem('role')}/user`,
        method: 'get',
        params: args,
        headers: { Authorization: `Bearer ${sessionStorage.getItem('token')}` },
      },
    },
  };
}

export function repairList(args) {
  return {
    type: 'REPAIR_LIST',
    payload: {
      request: {
        url: `/${sessionStorage.getItem('role')}/repair`,
        method: 'get',
        params: args,
        headers: { Authorization: `Bearer ${sessionStorage.getItem('token')}` },
      },
    },
  };
}

export function userUpdate(args) {
  return {
    type: 'USER_UPDATE',
    payload: {
      request: {
        url: `/${sessionStorage.getItem('role')}/user`,
        method: 'put',
        data: args,
        headers: { Authorization: `Bearer ${sessionStorage.getItem('token')}` },
      },
    },
  };
}

export function repairUpdate(args) {
  return {
    type: 'REPAIR_UPDATE',
    payload: {
      request: {
        url: `/${sessionStorage.getItem('role')}/repair`,
        method: 'put',
        data: args,
        headers: { Authorization: `Bearer ${sessionStorage.getItem('token')}` },
      },
    },
  };
}
