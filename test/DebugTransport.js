const debug = require('debug');
const winston = require('winston');

/**
 * Winston logger transport for debug module
 */
class DebugTransport extends winston.Transport {
  constructor(options) {
    super(options);
    this.debugInstance = debug(options.namespace);
  }

  log(level, msg, meta, cb) {
    this.debugInstance(`${msg}: %j`, meta);
    cb(null, true);
  }
}

module.exports = DebugTransport;
