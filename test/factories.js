const mochaTesterUser = () => ({
  name: 'Mocha Tester',
  username: 'mocha_tester',
  role: 'manager',
  password: 'asdf1234',
  email: 'mocha_tester@tester.com',
  photoUrl: null,
});

const mochaWorkerUser = () => ({
  name: 'Mocha Worker',
  username: 'mocha_worker',
  role: 'user',
  password: 'asdf1234',
  email: 'mocha_worker@tester.com',
  photoUrl: null,
});

module.exports = {
  mochaTesterUser,
  mochaWorkerUser,
};
