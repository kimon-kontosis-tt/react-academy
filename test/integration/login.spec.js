const _ = require('lodash');
const Client = require('../util').Client;
const expect = require('chai').expect;
const factories = require('../factories');
const mongoose = require('mongoose');
const proxyquire = require('proxyquire');

const client = new Client();

const mochaTesterUser = factories.mochaTesterUser();
const mochaTester = mochaTesterUser.username;

describe('#server #integration #login', () => {
  let app;
  before('should start the service', (done) => {
    app = proxyquire('../../server/index', {
      'webpack-dev-middleware': () => (req, res, next) => next(),
      'webpack-hot-middleware': () => (req, res, next) => next(),
    });
    app.once('hasStarted', done);
  });

  before(`should delete ${mochaTester} from the db`, async () => mongoose.models.User
    .find({ username: mochaTester })
    .remove());

  after('should stop the service', (done) => {
    app.server.close(done);
  });

  it('should fail to login without password', async () => {
    const err = await expect(client.post('/api/login', {
      username: 'test',
    })).to.be.rejected;

    expect(err.response.data).to.exist;
    expect(err.response.data.isValidationError).to.be.true;
    expect(err.response.status).to.eql(400);
  });

  it('should fail to login with an invalid user', async () => {
    const err = await expect(client.post('/api/login', {
      username: mochaTester,
      password: mochaTesterUser.password,
    })).to.be.rejected;

    expect(err.response.data).to.exist;
    expect(err.response.status).to.eql(401);
  });

  it(`should be able to save ${mochaTester} to the db and hash their password`, async () => {
    const userToSave = new mongoose.models.User(mochaTesterUser);
    await userToSave.save();

    const found = await mongoose.models.User
      .findOne({ username: mochaTester })
      .select('+hashedPassword');

    expect(found.hashedPassword).to.exist;
    expect(found.hashedPassword.length).to.be.above(10);
  });

  it('should fail to login with an invalid password', async () => {
    const err = await expect(client.post('/api/login', {
      username: mochaTester,
      password: 'invalid_password',
    })).to.be.rejected;

    expect(err.response.data).to.exist;
    expect(err.response.status).to.eql(401);
  });

  it('should fail to get the profile info with an invalid auth token', async () => {
    client.authToken =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoidXNlciIsImlhdCI6MTUwNjExNTYxM30.' +
      'lZZbAEE4yWdtGIGW1onE4AW1GDKVq0okTcuukRTwSRo';

    const err = await expect(client.get('/api/profile')).to.be.rejected;

    expect(err.response.data).to.exist;
    expect(err.response.status).to.eql(401);
  });

  it('should be able to login with a valid user and get the profile info', async () => {
    const response = await client.post('/api/login', {
      username: mochaTester,
      password: mochaTesterUser.password,
    });

    expect(response.data.token).to.exist;
    expect(response.data.token.length).to.be.above(20);
    expect(response.data.user._id).to.exist;
    expect(response.data.user.createdOn).to.exist;
    expect(_.omit(response.data.user, ['_id', 'createdOn'])).to.eql({
      email: 'mocha_tester@tester.com',
      name: 'Mocha Tester',
      username: 'mocha_tester',
      photoUrl: null,
      role: 'manager',
    });

    client.authToken = response.data.token;

    const response2 = await client.get('/api/profile');
    expect(response2.data).to.eql(response.data.user);
  });
});
