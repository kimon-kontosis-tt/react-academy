const _ = require('lodash');
const Client = require('../util').Client;
const expect = require('chai').expect;
const factories = require('../factories');
const mongoose = require('mongoose');
const proxyquire = require('proxyquire');

const client = new Client();

const mochaTesterUser = factories.mochaTesterUser();
const mochaWorkerUser = factories.mochaWorkerUser();
const mochaTester = mochaTesterUser.username;
const mochaWorker = mochaWorkerUser.username;

describe('#server #integration #user', () => {
  let app;
  before('should start the service', (done) => {
    app = proxyquire('../../server/index', {
      'webpack-dev-middleware': () => (req, res, next) => next(),
      'webpack-hot-middleware': () => (req, res, next) => next(),
    });
    app.once('hasStarted', done);
  });

  before(`should delete ${mochaTester} from the db`, async () => mongoose.models.User
    .find({ username: [mochaTester, mochaWorker] })
    .remove());

  after('should stop the service', (done) => {
    app.server.close(done);
  });

  it(`should be able to /register ${mochaTester} with a role user`, async () => {
    const result = await client.post('/api/register', _.defaults({ role: 'user' }, mochaTesterUser));
    expect(result.data.success).to.be.true;
  });

  it(`should be able to /register ${mochaWorker} with a role user`, async () => {
    const result = await client.post('/api/register', mochaWorkerUser);
    expect(result.data.success).to.be.true;
  });

  it(`should be able to /login ${mochaTester}`, async () => {
    const response = await client.post('/api/login', {
      username: mochaTester,
      password: mochaTesterUser.password,
    });
    expect(response.data.token).to.exist;
    client.authToken = response.data.token;
  });

  it('should get 404 trying to delete /user with an empty id: role security', async () => {
    await mongoose.models.User.findOne({ username: mochaWorker });
    const err = await expect(client.delete('/api/manager/user?id=')).to.be.rejected;
    expect(err.response.status).to.eql(401);
  });

  it(`should get 404 trying to delete /user ${mochaWorker}`, async () => {
    const worker = await mongoose.models.User.findOne({ username: mochaWorker });
    const err = await expect(client.delete(`/api/manager/user?id=${worker._id}`)).to.be.rejected;
    expect(err.response.status).to.eql(401);
  });
});
