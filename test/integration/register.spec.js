const Client = require('../util').Client;
const expect = require('chai').expect;
const factories = require('../factories');
const mongoose = require('mongoose');
const proxyquire = require('proxyquire');

const client = new Client();

const mochaTesterUser = factories.mochaTesterUser();
const mochaTester = mochaTesterUser.username;

describe('#server #integration #register', () => {
  let app;
  before('should start the service', (done) => {
    app = proxyquire('../../server/index', {
      'webpack-dev-middleware': () => (req, res, next) => next(),
      'webpack-hot-middleware': () => (req, res, next) => next(),
    });
    app.once('hasStarted', done);
  });

  before(`should delete ${mochaTester} from the db`, async () => mongoose.models.User
    .find({ username: mochaTester })
    .remove());

  after('should stop the service', (done) => {
    app.server.close(done);
  });

  it('should register a new user', async () => {
    const {
      username,
      password,
      email,
      name,
      role,
    } = mochaTesterUser;

    const response = await client.post('/api/register', {
      username,
      password,
      email,
      name,
      role,
    });

    expect(response.data).to.exist;
    expect(response.data.success).to.be.true;
    expect(response.status).to.eql(201);
  });

  it('should fail to register the same user twice', async () => {
    const {
      username,
      password,
      email,
      name,
      role,
    } = mochaTesterUser;

    const err = await expect(client.post('/api/register', {
      username,
      password,
      email,
      name,
      role,
    })).to.be.rejected;

    expect(err.response.data).to.exist;
    expect(err.response.status).to.eql(401);
  });

  it('should check if the username is available', async () => {
    expect((await client.get(`/api/available?username=${mochaTesterUser.username}`)).data.available).to.be.false;
    expect((await client.get(`/api/available?email=${mochaTesterUser.email}`)).data.available).to.be.false;
    expect((await client.get(`/api/available?username=e${mochaTesterUser.username}`)).data.available).to.be.true;
    expect((await client.get(`/api/available?email=e${mochaTesterUser.email}`)).data.available).to.be.true;
  });
});
