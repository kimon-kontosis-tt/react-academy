module.exports = {
  'step one': function (browser) {
    browser
      .url('http://localhost:8080/')
      .getLogTypes((result) => {
        console.log(result);
      })
      .getLog('browser', (result) => {
        console.log(result);
      })
      .waitForElementVisible('body', 1000)
      .waitForElementVisible('h1', 1000)
      .assert.elementPresent('body')
      .assert.elementPresent('h1')
      .assert.containsText('h1', 'React Academy Auto Repair')
      .end();
  },
};
