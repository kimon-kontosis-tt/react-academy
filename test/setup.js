const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const DebugTransport = require('./DebugTransport');
const winston = require('winston');

chai.use(chaiAsPromised);

// Use debug transport for logging instead of console
winston.add(DebugTransport, { namespace: 'react-academy' });
winston.remove(winston.transports.Console);
